///////////////////////////////////////////////////////////////////////////
// Copyright © Esri. All Rights Reserved.
//
// Licensed under the Apache License Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////
define([
  "dojo/Stateful",
  'dojo',
  'dijit',
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/array',
  'dojo/_base/html',
  'dojo/query',
  'dojo/aspect',
  'dojo/i18n!esri/nls/jsapi',
  'dojo/dom',
  'dojo/dom-construct',
  'dojo/dom-class',
  'dojo/dom-style',
  'dojo/on',
  'dojo/keys',
  'dojo/json',
  'dojo/topic',
  'dijit/_WidgetsInTemplateMixin',
  'jimu/BaseWidget',
  'jimu/LayerInfos/LayerInfos',
  'jimu/dijit/Message',
  "esri/request",
  "esri/dijit/editing/TemplatePicker",
  "esri/dijit/AttributeInspector",
  "esri/toolbars/draw",
  "esri/toolbars/edit",
  "esri/tasks/query",
  "esri/graphic",
  "esri/layers/FeatureLayer",
  "dojo/promise/all",
  "dojo/Deferred",
  "esri/symbols/SimpleMarkerSymbol",
  "esri/symbols/SimpleLineSymbol",
  "esri/symbols/SimpleFillSymbol",
  "esri/Color",
  "esri/geometry/Circle",
  "esri/renderers/SimpleRenderer",
  "esri/dijit/PopupTemplate",
  "esri/geometry/jsonUtils",
  "esri/geometry/Polyline",
  "esri/geometry/Polygon",
  "esri/tasks/RelationshipQuery",
  "dijit/registry",
  "./PresetAllFields",
  "./utils",
  "./presetUtils",
  "./presetBuilderBackwardCompatibility",
  "./smartAttributes",
  "./attributeInspectorTools",
  "jimu/dijit/CheckBox",
  "dijit/form/Button",
  "dijit/form/DropDownButton",
  'dijit/DropDownMenu',
  "dijit/MenuItem",
  'dijit/form/DateTextBox',
  'dijit/form/NumberSpinner',
  'dijit/form/NumberTextBox',
  'dijit/form/FilteringSelect',
  'dijit/form/TextBox',
  'dijit/form/ValidationTextBox',
  'dijit/form/TimeTextBox',
  'dijit/form/Select',
  "dijit/Editor",
  "dijit/form/SimpleTextarea",
  'dojo/store/Memory',
  'dojo/date/stamp',
  "dojo/dom-attr",
  "jimu/dijit/Popup",
  "esri/lang",
  "esri/renderers/jsonUtils",
  "dojox/html/entities",
  'jimu/dijit/EditorXssFilter',
  'jimu/utils',
  'jimu/portalUtils',
  'jimu/portalUrlUtils',
  'jimu/SelectionManager',
  'jimu/dijit/LoadingIndicator',
  'esri/tasks/GeometryService',
  'esri/geometry/geometryEngine',
  'esri/arcgis/Portal',
  "esri/dijit/LocateButton",
  "esri/geometry/Point",
  'esri/SpatialReference',
  "dijit/focus",
  'jimu/dijit/FeatureSetChooserForMultipleLayers',
  "dijit/Tooltip"
],
  function (
    Stateful,
    dojo,
    dijit,
    declare,
    lang,
    array,
    html,
    query,
    aspect,
    esriBundle,
    dom,
    domConstruct,
    domClass,
    domStyle,
    on,
    keys,
    JSON,
    topic,
    _WidgetsInTemplateMixin,
    BaseWidget,
    LayerInfos,
    Message,
    esriRequest,
    TemplatePicker,
    AttributeInspector,
    Draw,
    Edit,
    Query,
    Graphic,
    FeatureLayer,
    all,
    Deferred,
    SimpleMarkerSymbol,
    SimpleLineSymbol,
    SimpleFillSymbol,
    Color,
    Circle,
    SimpleRenderer,
    PopupTemplate,
    geometryJsonUtil,
    Polyline,
    Polygon,
    RelationshipQuery,
    registry,
    PresetAllFields,
    editUtils,
    presetUtils,
    presetBuilderBackwardCompatibility,
    smartAttributes,
    attributeInspectorTools,
    CheckBox,
    Button,
    DropDownButton,
    DropDownMenu,
    MenuItem,
    DateTextBox,
    NumberSpinner,
    NumberTextBox,
    FilteringSelect,
    TextBox,
    ValidationTextBox,
    TimeTextBox,
    Select,
    Editor,
    SimpleTextarea,
    Memory,
    dojoStamp,
    domAttr,
    Popup,
    esriLang,
    rendererJsonUtils,
    entities,
    EditorXssFilter,
    utils,
    portalUtils,
    portalUrlUtils,
    SelectionManager,
    LoadingIndicator,
    GeometryService,
    GeometryEngine,
    esriPortal,
    coordinateUtils,
    AddressUtils,
    Intersection,
    LocateButton,
    Point,
    SpatialReference,
    focusUtil,
    FeatureSetChooserForMultipleLayers,
    CopyFeatures,
    ValuePicker,
    String,
    requiredFields,
    Tooltip) {
      var validadores=[]
    var clazz = declare([BaseWidget, _WidgetsInTemplateMixin], {
      baseClass: 'jimu-widget-about',
      selectEjecutores: null,
      btnAsignacion: null,
      featSelected: null,
      validador: null,
      postCreate: function () {
        this.inherited(arguments);
      },

      startup: function () {
        this.inherited(arguments);
        this.visualStartup();
        this.btnCrearCirculo = document.getElementById('btn-CrearCirculo')
        this.btnFinalizar = document.getElementById('btn-Finalizar')
        this.btnCrearCirculo.addEventListener('click', lang.hitch(this, validadores.push(1)))
        //this.btnFinalizar.addEventListener('click', lang.hitch(this, validadores.pop()))
        
        var symbol = new SimpleMarkerSymbol(
          SimpleMarkerSymbol.STYLE_CIRCLE,
          12,
          new SimpleLineSymbol(
            SimpleLineSymbol.STYLE_NULL,
            new Color([247, 34, 101, 0.9]),
            1
          ),
          new Color([207, 34, 171, 0.5])
        );
        var featureLayer = new FeatureLayer("https://services.arcgis.com/deQSb0Gn7gDPf3uV/arcgis/rest/services/PREDIOS_ZONA_ESTUDIO_5G/FeatureServer/0", {
          outFields: ["CODIGO_PREDIO", "PUNTUACION_FAMILIA", "Total"]
        });
        featureLayer.setSelectionSymbol(symbol);
        var nullSymbol = new SimpleMarkerSymbol().setSize(0);
        featureLayer.setRenderer(new SimpleRenderer(nullSymbol));
        this.map.addLayer(featureLayer)
        console.log(validadores)
        debugger
        var circleSymb = new SimpleFillSymbol(
          SimpleFillSymbol.STYLE_NULL,
          new SimpleLineSymbol(
            SimpleLineSymbol.STYLE_SHORTDASHDOTDOT,
            new Color([105, 105, 105]),
            2
          ), new Color([255, 255, 0, 0.25])
        );
        var circle;



        this.map.on('click', lang.hitch(this, function (evt) {
          circle = new Circle({
            center: evt.mapPoint,
            geodesic: true,
            radius: 0.5,
            radiusUnit: "esriMiles",
            spatialReference: { wkid: 4326 }
          });
          this.map.graphics.clear();
          var graphic = new Graphic(circle, circleSymb);
          this.map.graphics.add(graphic);
          var query = new Query();
          query.geometry = circle.getExtent();
          // Use a fast bounding box query. It will only go to the server if bounding box is outside of the visible map.
          featureLayer.queryFeatures(query, selectInBuffer);

          //this.lblPretSelected.innerHTML = this.featSelected.attributes.CODIGO_PERTURBACION
          function selectInBuffer(response) {
            var feature;
            var features = response.features;
            var inBuffer = [];
            debugger
            // Filter out features that are not actually in buffer, since we got all points in the buffer's bounding box
            for (var i = 0; i < features.length; i++) {
              feature = features[i];

              console.log(feature.attributes.OBJECTID)
              if (feature.attributes.Total > 50) {
                debugger
                inBuffer.push(feature.attributes.OBJECTID);
              }
              //inBuffer.push(feature.attributes.OBJECTID);
              if (circle.contains(feature.geometry)) {
                debugger
                inBuffer.push(feature.attributes.OBJECTID);
              }
            }
            var query = new Query();
            query.objectIds = inBuffer;
            debugger
            // Use an objectIds selection query (should not need to go to the server)
            featureLayer.selectFeatures(query, FeatureLayer.SELECTION_NEW, function (results) {
            });
          }

        }))
        //this.btnCrearCirculo.addEventListener('click', lang.hitch(this, this.crearBufferCircle))

        this.btnFinalizar.addEventListener('click', lang.hitch(this, this.limpiarMapa))
        //this.toggleAssignBtn(this.selectCapa.value)



        //this.selectCapa.addEventListener('click', lang.hitch(this, this.assignEjecutor))


      },
      visualStartup: function () {

        /*
        if(this.selectCapa.innerHTML=="Zonas de amenaza"){
          this.Amenaza.disabled = true;
        }
        */
        console.log(this.selectCapa)
      },

      toggleAssignBtn: function () {
      },

      onOpen: function () {
        this.isOpen = true;
        //resolve issue #15086 when network is so slow.
        setTimeout(lang.hitch(this, function () {
          this.isOpen = false;
        }), 50);
      },

      crearBufferCircle: async function () {

        var symbol = new SimpleMarkerSymbol(
          SimpleMarkerSymbol.STYLE_CIRCLE,
          12,
          new SimpleLineSymbol(
            SimpleLineSymbol.STYLE_NULL,
            new Color([247, 34, 101, 0.9]),
            1
          ),
          new Color([207, 34, 171, 0.5])
        );
        var featureLayer = new FeatureLayer("https://services.arcgis.com/deQSb0Gn7gDPf3uV/arcgis/rest/services/PREDIOS_ZONA_ESTUDIO_5G/FeatureServer/0", {
          outFields: ["CODIGO_PREDIO", "PUNTUACION_FAMILIA", "Total"]
        });
        featureLayer.setSelectionSymbol(symbol);
        var nullSymbol = new SimpleMarkerSymbol().setSize(0);
        featureLayer.setRenderer(new SimpleRenderer(nullSymbol));
        this.map.addLayer(featureLayer)
        debugger
        var circleSymb = new SimpleFillSymbol(
          SimpleFillSymbol.STYLE_NULL,
          new SimpleLineSymbol(
            SimpleLineSymbol.STYLE_SHORTDASHDOTDOT,
            new Color([105, 105, 105]),
            2
          ), new Color([255, 255, 0, 0.25])
        );
        var circle;



        this.map.on('click', lang.hitch(this, function (evt) {
          circle = new Circle({
            center: evt.mapPoint,
            geodesic: true,
            radius: 0.5,
            radiusUnit: "esriMiles",
            spatialReference: { wkid: 4326 }
          });
          this.map.graphics.clear();
          var graphic = new Graphic(circle, circleSymb);
          this.map.graphics.add(graphic);
          var query = new Query();
          query.geometry = circle.getExtent();
          // Use a fast bounding box query. It will only go to the server if bounding box is outside of the visible map.
          featureLayer.queryFeatures(query, selectInBuffer);

          //this.lblPretSelected.innerHTML = this.featSelected.attributes.CODIGO_PERTURBACION
          function selectInBuffer(response) {
            var feature;
            var features = response.features;
            var inBuffer = [];
            debugger
            // Filter out features that are not actually in buffer, since we got all points in the buffer's bounding box
            for (var i = 0; i < features.length; i++) {
              feature = features[i];

              console.log(feature.attributes.OBJECTID)
              if (feature.attributes.Total > 50) {
                debugger
                inBuffer.push(feature.attributes.OBJECTID);
              }
              //inBuffer.push(feature.attributes.OBJECTID);
              if (circle.contains(feature.geometry)) {
                debugger
                inBuffer.push(feature.attributes.OBJECTID);
              }
            }
            var query = new Query();
            query.objectIds = inBuffer;
            
            // Use an objectIds selection query (should not need to go to the server)
            featureLayer.selectFeatures(query, FeatureLayer.SELECTION_NEW, function (results) {
            });
          }

        }))

      },

      limpiarMapa: function () {
        this.map.graphics.clear();
        var featureLayer = new FeatureLayer("https://services.arcgis.com/deQSb0Gn7gDPf3uV/arcgis/rest/services/PREDIOS_ZONA_ESTUDIO_5G/FeatureServer/0", {
          outFields: ["CODIGO_PREDIO", "PUNTUACION_FAMILIA", "Total"]
        });
        this.map.removeLayer(featureLayer)
      },
      _getIntersectZonasRiesgo: async function (polygon) {

      },

      _getIntersectCuerposAgua: async function (polygon) {

      },

      successMsg: function () {
        /*
        // actualizar el estado del reporte ha asignado
        let perturbPointFl = new esri.layers.FeatureLayer(this.appConfig.perturbacionesConfig.pointLyr)
        let query = new esri.tasks.Query();
        query.where = "CODIGO_REPORTE = '" + this.featSelected.attributes.CODIGO_REPORTE + "'"
        query.outFields = ["OBJECTID", "ESTADO_REPORTE"]
        perturbPointFl.queryFeatures(query, lang.hitch(this, function (featureSet) {
          let feat = featureSet.features[0]
          feat.attributes.ESTADO_REPORTE = "ASI"
          perturbPointFl.applyEdits(null, [feat])

          let popup = new Popup({
            hasTitle: false,
            width: 340,
            maxHeight: 200,
            autoHeight: true,
            content: "Perturbación asignada exitosamente",
            buttons: [{
              label: "Ok",
              onClick: lang.hitch(this, function () {
                popup.close()
              })
            }]
          })

          this.lblPertSelected.innerHTML = "Clic en el mapa sobre la perturbación"
          this.btnCrearCirculo.disabled = true
        }), function (err) {
          alert('Error al consultar el reporte de la perturbación')
          console.log(err)
        })
        */
      },

      loadEjecutores: function () {

      }
    });
    return clazz;
  });