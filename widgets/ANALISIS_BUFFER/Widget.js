///////////////////////////////////////////////////////////////////////////
// Copyright © Esri. All Rights Reserved.
//
// Licensed under the Apache License Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////
define([
  "dojo/Stateful",
  'dojo',
  'dijit',
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/array',
  'dojo/_base/html',
  'dojo/query',
  'dojo/aspect',
  'dojo/i18n!esri/nls/jsapi',
  'dojo/dom',
  'dojo/dom-construct',
  'dojo/dom-class',
  'dojo/dom-style',
  'dojo/on',
  'dojo/keys',
  'dojo/json',
  'dojo/topic',
  'dijit/_WidgetsInTemplateMixin',
  'jimu/BaseWidget',
  'jimu/LayerInfos/LayerInfos',
  'jimu/dijit/Message',
  "esri/request",
  "esri/dijit/editing/TemplatePicker",
  "esri/dijit/AttributeInspector",
  "esri/toolbars/draw",
  "esri/toolbars/edit",
  "esri/tasks/query",
  "esri/graphic",
  "esri/layers/FeatureLayer",
  "dojo/promise/all",
  "dojo/Deferred",
  "esri/symbols/SimpleMarkerSymbol",
  "esri/symbols/SimpleLineSymbol",
  "esri/symbols/SimpleFillSymbol",
  "esri/Color",
  "esri/dijit/PopupTemplate",
  "esri/geometry/jsonUtils",
  "esri/geometry/Polyline",
  "esri/geometry/Polygon",
  "esri/tasks/RelationshipQuery",
  "dijit/registry",
  "./PresetAllFields",
  "./utils",
  "./presetUtils",
  "./presetBuilderBackwardCompatibility",
  "./smartAttributes",
  "./attributeInspectorTools",
  "jimu/dijit/CheckBox",
  "dijit/form/Button",
  "dijit/form/DropDownButton",
  'dijit/DropDownMenu',
  "dijit/MenuItem",
  'dijit/form/DateTextBox',
  'dijit/form/NumberSpinner',
  'dijit/form/NumberTextBox',
  'dijit/form/FilteringSelect',
  'dijit/form/TextBox',
  'dijit/form/ValidationTextBox',
  'dijit/form/TimeTextBox',
  'dijit/form/Select',
  "dijit/Editor",
  "dijit/form/SimpleTextarea",
  'dojo/store/Memory',
  'dojo/date/stamp',
  "dojo/dom-attr",
  "jimu/dijit/Popup",
  "esri/lang",
  "esri/renderers/jsonUtils",
  "dojox/html/entities",
  'jimu/dijit/EditorXssFilter',
  'jimu/utils',
  'jimu/portalUtils',
  'jimu/portalUrlUtils',
  'jimu/SelectionManager',
  'jimu/dijit/LoadingIndicator',
  'esri/tasks/GeometryService',
  'esri/geometry/geometryEngine',
  'esri/arcgis/Portal',
  "esri/dijit/LocateButton",
  "esri/geometry/Point",
  'esri/SpatialReference',
  "dijit/focus",
  'jimu/dijit/FeatureSetChooserForMultipleLayers',
  "dijit/Tooltip"
],
  function (
    Stateful,
    dojo,
    dijit,
    declare,
    lang,
    array,
    html,
    query,
    aspect,
    esriBundle,
    dom,
    domConstruct,
    domClass,
    domStyle,
    on,
    keys,
    JSON,
    topic,
    _WidgetsInTemplateMixin,
    BaseWidget,
    LayerInfos,
    Message,
    esriRequest,
    TemplatePicker,
    AttributeInspector,
    Draw,
    Edit,
    Query,
    Graphic,
    FeatureLayer,
    all,
    Deferred,
    SimpleMarkerSymbol,
    SimpleLineSymbol,
    SimpleFillSymbol,
    Color,
    PopupTemplate,
    geometryJsonUtil,
    Polyline,
    Polygon,
    RelationshipQuery,
    registry,
    PresetAllFields,
    editUtils,
    presetUtils,
    presetBuilderBackwardCompatibility,
    smartAttributes,
    attributeInspectorTools,
    CheckBox,
    Button,
    DropDownButton,
    DropDownMenu,
    MenuItem,
    DateTextBox,
    NumberSpinner,
    NumberTextBox,
    FilteringSelect,
    TextBox,
    ValidationTextBox,
    TimeTextBox,
    Select,
    Editor,
    SimpleTextarea,
    Memory,
    dojoStamp,
    domAttr,
    Popup,
    esriLang,
    rendererJsonUtils,
    entities,
    EditorXssFilter,
    utils,
    portalUtils,
    portalUrlUtils,
    SelectionManager,
    LoadingIndicator,
    GeometryService,
    GeometryEngine,
    esriPortal,
    coordinateUtils,
    AddressUtils,
    Intersection,
    LocateButton,
    Point,
    SpatialReference,
    focusUtil,
    FeatureSetChooserForMultipleLayers,
    CopyFeatures,
    ValuePicker,
    String,
    requiredFields,
    Tooltip) {
    var clazz = declare([BaseWidget, _WidgetsInTemplateMixin], {
      baseClass: 'jimu-widget-about',
      selectEjecutores: null,
      btnAsignacion: null,
      featSelected: null,

      postCreate: function () {
        this.inherited(arguments);
      },

      startup: function () {
        this.inherited(arguments);
        this.visualStartup();
        this.btnAsignacion = document.getElementById('btn-asignacion')
        this.selectEjecutores = document.getElementById("select-radio-influencia")
        this.selectCapa = document.getElementById("select-Capa")
        this.Amenaza = document.getElementById("select-Amenaza")
        this.TipoAmenaza = document.getElementById("Tipo_de_Amenaza")
        this.lblPretSelected = document.getElementById('lbl-predio-selected')
        this.selecTipo = document.getElementById('select-tipo-pert');
        this.Codigo_predio = document.getElementById("Codigo_predio")
        this.Campo= document.getElementById("select-campo")
        this.consulta= document.getElementById("select-tipo-consulta")
        
        this.lblPertSelected = document.getElementById('lbl-pert-selected')
        this.IntersectPrediosaptos = document.getElementById("lbl-CantidadPredios")

        this.CantidadPredios = document.getElementById("lbl-CantidadPredios")

        this.TipoAmenaza.style.display = "none"
        this.btnAsignacion.disabled = false
        this.Amenaza.style.display = "none";
        this.lblPretSelected.style.display = "none";
        this.Codigo_predio.style.display = "none"
        this.loadEjecutores()
        //this.toggleAssignBtn(this.selectCapa.value)
        this.map.on('click', lang.hitch(this, function (response) {

          this.featSelected = this.map.infoWindow.getSelectedFeature()
          
          /*
          if (this.featSelected._layer.id == "PREDIOS_ZONA_ESTUDIO_5G_1184") {
            this.lblPretSelected.innerHTML = this.featSelected.attributes.CODIGO_PREDIO
          }
          */
          // se seleccionó una capa de perturbación? 
          console.log(this.map.infoWindow.getSelectedFeature())
          //this.lblPretSelected.innerHTML = this.featSelected.attributes.CODIGO_PERTURBACION


        }))

        this.btnAsignacion.addEventListener('click', lang.hitch(this, this.assignEjecutor))

        this.IntersectPrediosaptos.addEventListener('click', lang.hitch(this, this.limpiarMapa))

        //this.selectCapa.addEventListener('click', lang.hitch(this, this.assignEjecutor))
        //this.Amenaza.addEventListener('click', lang.hitch(this, this.assignEjecutor))
        this.selectCapa.addEventListener('change', lang.hitch(this, function (domNode) {
          let value = domNode.target.value
          console.log(value)
          debugger
          if (this.selectCapa.value == "https://serviciosgis.catastrobogota.gov.co/otrosservicios/rest/services/emergencias/gestionriesgos/MapServer/1" || this.selectCapa.value == "https://serviciosgis.catastrobogota.gov.co/otrosservicios/rest/services/emergencias/gestionriesgos/MapServer/2") {
            this.TipoAmenaza.style.display = "block";
            this.Amenaza.style.display = "block";

          }
          else {
            this.TipoAmenaza.style.display = "none";
            this.Amenaza.style.display = "none";
          }

          //this.toggleAssignBtn(value)
        }))
        this.Campo.addEventListener('change', lang.hitch(this, function (domNode) {
          let value = domNode.target.value

          //this.toggleAssignBtn(value)
        }))
        this.consulta.addEventListener('change', lang.hitch(this, function (domNode) {
          let value = domNode.target.value
          //this.toggleAssignBtn(value)
        }))
        /*
        this.selecTipo.addEventListener('change', lang.hitch(this, function (domNode) {
          let value = domNode.target.value
          console.log(value)
          debugger
          if (this.selecTipo.value == "Si") {
            this.Codigo_predio.style.display = "block"
            this.lblPretSelected.style.display = "block";
          }
          else {
            this.Codigo_predio.style.display = "none"
            this.lblPretSelected.style.display = "none";
          }

          //this.toggleAssignBtn(value)
        }))
        */

      },
      visualStartup: function () {
        /*
        if(this.selectCapa.innerHTML=="Zonas de amenaza"){
          this.Amenaza.disabled = true;
        }
        */
        console.log(this.selectCapa)
      },

      limpiarMapa: function() {
        this.map.graphics.clear();
      },

      toggleAssignBtn: function (codEjecutor) {
        this.btnAsignacion.disabled = codEjecutor === '' || !codEjecutor
      },

      onOpen: function () {
        this.isOpen = true;
        //resolve issue #15086 when network is so slow.
        setTimeout(lang.hitch(this, function () {
          this.isOpen = false;
        }), 50);
      },

      assignEjecutor: async function () {
        this.map.graphics.clear();
        var vias_g5 = new esri.layers.FeatureLayer("https://sig.ani.gov.co/arcgissrv/rest/services/SigAni/ConcesionesCarreteras/MapServer/0");
        //var refreshTipificacion = new esri.layers.FeatureLayer(this.appConfig.perturbacionesConfig.TipificacionLyr)
        //refreshTipificacion.refresh()
        var layerUrl = "https://sig.ani.gov.co/arcgissrv/rest/services/SigAni/ConcesionesCarreteras/MapServer/0"
        var codigo = "5g"
        let outValue = "";
        let where = "where=GEN='" + codigo + "'";
        let outFields = "outFields=NOMBRE,GEN  ";
        let returnGeometry = "returnGeometry=true";
        var wokidsalida = "outSR=4326"
        let f = "f=pjson";
        let parameters = [where, outFields, wokidsalida, returnGeometry, f].join("&");
        let url = `${layerUrl}/query?${parameters}`;
        //console.log("url", url);
        let xhr = new XMLHttpRequest();
        xhr.open("GET", url, false);
        xhr.send();
        let response = JSON.parse(xhr.responseText);
        debugger
        var actualCoords = [];
        response.features.forEach(function (value, index) {
          actualCoords.push(value.geometry["paths"]);
        });
        var PolygonEnd = [];
        for (var i = 0; i < actualCoords.length; i++) {
          var geometryPol = new Polygon({
            rings: actualCoords[i],
            spatialReference: { wkid: 4326 }
          });
          PolygonEnd.push(geometryPol);
        }
        var valorBuffer = parseInt(this.selectEjecutores.value, 10);
        debugger
        var convexHullP = GeometryEngine.geodesicBuffer(PolygonEnd, [valorBuffer], "meters", true);

        var graphic = new Graphic(convexHullP[0], new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
          new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASHDOT,
            new Color([120, 255, 0]), 2), new Color([255, 255, 0, 0.25])
        ));

        this.map.graphics.add(graphic);




        if (this.selectCapa.value == "https://serviciosgis.catastrobogota.gov.co/otrosservicios/rest/services/emergencias/gestionriesgos/MapServer/1" || this.selectCapa.value == "https://serviciosgis.catastrobogota.gov.co/otrosservicios/rest/services/emergencias/gestionriesgos/MapServer/2") {
            var CantidadPredios=[];
          var Interseccion = await this._getIntersectZonasRiesgo(convexHullP[0])
          var PolygonEndInter = [];
          for (var i = 0; i < Interseccion.features.length; i++) {
            var newpolygon = new Polygon({
              rings: Interseccion.features[i].geometry.rings,
              spatialReference: { wkid: 4326 }
            });
            PolygonEndInter.push(newpolygon)
          }
          //var convexHullP = GeometryEngine.union(PolygonEndInter);
          var featureLayer = new FeatureLayer("https://services.arcgis.com/deQSb0Gn7gDPf3uV/arcgis/rest/services/PREDIOS_ZONA_ESTUDIO_5G/FeatureServer/0", {
            outFields: ["CODIGO_PREDIO", "PUNTUACION_FAMILIA","OBJECTID","Total"]
          });
          var valorEvaluar = parseInt(this.selecTipo.value, 10);
          var query = new Query();
          var camopoevaluado=this.Campo.value;
          var tipoconsulta=this.consulta.value
          var queryp=camopoevaluado+tipoconsulta+this.selecTipo.value
          
          query.where = queryp
          query.outFields = ["PUNTUACION_FAMILIA", "CODIGO_PREDIO ","Total","OBJECTID"];
          var results = await featureLayer.queryFeatures(query);
          var PolygonPredios = [];
          //array.forEach(results.features, lang.hitch(this, function (geometry) {
           //   var prueba = geometry
            //  debugger
          //}))

          for (var i = 0; i < results.features.length; i++) {
            var newpolygon = new Polygon({
              rings: results.features[i].geometry.rings,
              spatialReference: { wkid: 4326 },
              declaredClass: results.features[i].attributes["CODIGO_PREDIO"]+"-"+results.features[i].attributes["PUNTUACION_FAMILIA"]
            });
            PolygonPredios.push(newpolygon)
          }

          var Predios = GeometryEngine.union(PolygonPredios);
          debugger

          for (var i = 0; i < PolygonEndInter.length; i++) {

            var convexHull = GeometryEngine.intersect(PolygonEndInter[i], convexHullP[0]);

            const popupTemplate = {
              title: "Zona de Amenaza",
              content: "Zona de" + this.Amenaza.value +" Predio con puntuación mayor a"+valorEvaluar
            }
            const attributes = {
              Name: "Graphic",
              Description: "I am a polygon"
            }
            var graphic = new Graphic(convexHull, new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
              new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASHDOT,
                new Color([255, 0, 0]), 2), new Color([255, 255, 0, 0.25])
            ), attributes, popupTemplate);
            this.map.graphics.add(graphic);
            var dict = new Object();
            var dict = [];
            array.forEach(results.features, lang.hitch(this, function (geometry) { 
            
              var newpolygon = new Polygon({
                rings: geometry.geometry.rings,
                spatialReference: { wkid: 4326 },
                declaredClass: results.features[i].attributes["CODIGO_PREDIO"]+"-"+results.features[i].attributes["PUNTUACION_FAMILIA"]
              }); 
              var InterseccionPredios = GeometryEngine.intersect(convexHull, newpolygon);

              if (InterseccionPredios != null) {
                CantidadPredios.push(1);
                dict.push({
                  CODIGO_PREDIO: geometry.attributes.CODIGO_PREDIO,
                  Puntuacion: geometry.attributes.Total
                });
              
                const popupTemplate = {
                  title: "Zona de Amenaza",
                  content: "Zona de " + this.Amenaza.value+" Puntuación igual a: "+geometry.attributes.Total +"Url DashBoard: https://udistritalfjc.maps.arcgis.com/apps/dashboards/e9b4814b7e2447219aae5a263ea67e1a#CodigoPredio="+geometry.attributes.CODIGO_PREDIO
                }
                const attributes = {
                  Name: "Graphic",
                  Description: "I am a polygon"
                }
                var graphic = new Graphic(InterseccionPredios, new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
                  new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASHDOT,
                    new Color([0, 0, 0]), 2), new Color([255, 255, 0, 0.25])
                ), attributes, popupTemplate);
                this.map.graphics.add(graphic);
              }
            }))

            this.CantidadPredios.innerHTML=JSON.stringify(dict)




            /*
            var actualCoords = [];
            response.features.forEach(function (value, index) {
              actualCoords.push(value.geometry["paths"]);
            });
            var PolygonEnd = [];
            for (var i = 0; i < actualCoords.length; i++) {
              var geometryPol = new Polygon({
                rings: actualCoords[i],
                spatialReference: { wkid: 4326 }
              });
              PolygonEnd.push(geometryPol);
            }
            for(var i=0;i<PolygonEnd.length;i++){
              var convexHull = GeometryEngine.intersect(PolygonEndInter[i], convexHull);
              var graphic = new Graphic(convexHull, new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASHDOT,
                  new Color([100, 0, 0]), 2), new Color([255, 255, 0, 0.25])
              ));
              this.map.graphics.add(graphic);
            }
            */


          }
        } else {
          var Interseccion = await this._getIntersectCuerposAgua(convexHullP[0])
          var PolygonEndInter = [];
          for (var i = 0; i < Interseccion.features.length; i++) {
            var newpolygon = new Polygon({
              rings: Interseccion.features[i].geometry.rings,
              spatialReference: { wkid: 4326 }
            });
            PolygonEndInter.push(newpolygon)
          }
          
          var featureLayer = new FeatureLayer("https://services.arcgis.com/deQSb0Gn7gDPf3uV/arcgis/rest/services/PREDIOS_ZONA_ESTUDIO_5G/FeatureServer/0", {
            outFields: ["CODIGO_PREDIO", "PUNTUACION_FAMILIA"]
          });
          var valorEvaluar = parseInt(this.selecTipo.value, 10);
          var query = new Query();
          var camopoevaluado=this.Campo.value;
          var tipoconsulta=this.consulta.value
          var queryp=camopoevaluado+tipoconsulta+this.selecTipo.value
          debugger
          query.where = queryp
          query.outFields = ["PUNTUACION_FAMILIA", "CODIGO_PREDIO "];
          var results = await featureLayer.queryFeatures(query);
          var PolygonPredios = [];
          //array.forEach(results.features, lang.hitch(this, function (geometry) {
           //   var prueba = geometry
            //  debugger
          //}))

          for (var i = 0; i < results.features.length; i++) {
            var newpolygon = new Polygon({
              rings: results.features[i].geometry.rings,
              spatialReference: { wkid: 4326 },
              declaredClass: results.features[i].attributes["CODIGO_PREDIO"]+"-"+results.features[i].attributes["PUNTUACION_FAMILIA"]
            });
            PolygonPredios.push(newpolygon)
          }

          var Predios = GeometryEngine.union(PolygonPredios);
          debugger

          for (var i = 0; i < PolygonEndInter.length; i++) {

            var convexHull = GeometryEngine.intersect(PolygonEndInter[i], convexHullP[0]);

            const popupTemplate = {
              title: "Zona de Amenaza",
              content: "Zona de" + this.Amenaza.value +" Predio con puntuación mayor a"+valorEvaluar 
            }
            const attributes = {
              Name: "Graphic",
              Description: "I am a polygon"
            }
            var graphic = new Graphic(convexHull, new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
              new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASHDOT,
                new Color([255, 0, 0]), 2), new Color([255, 255, 0, 0.25])
            ), attributes, popupTemplate);
            this.map.graphics.add(graphic);


            var InterseccionPredios = GeometryEngine.intersect(convexHull, Predios);
            if (InterseccionPredios != null) {
              debugger
              const popupTemplate = {
                title: "Zona de Amenaza",
                content: "Zona de" + this.Amenaza.value
              }
              const attributes = {
                Name: "Graphic",
                Description: "I am a polygon"
              }
              var graphic = new Graphic(InterseccionPredios, new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASHDOT,
                  new Color([0, 0, 0]), 2), new Color([255, 255, 0, 0.25])
              ), attributes, popupTemplate);
              this.map.graphics.add(graphic);
            }


            /*
            var actualCoords = [];
            response.features.forEach(function (value, index) {
              actualCoords.push(value.geometry["paths"]);
            });
            var PolygonEnd = [];
            for (var i = 0; i < actualCoords.length; i++) {
              var geometryPol = new Polygon({
                rings: actualCoords[i],
                spatialReference: { wkid: 4326 }
              });
              PolygonEnd.push(geometryPol);
            }
            for(var i=0;i<PolygonEnd.length;i++){
              var convexHull = GeometryEngine.intersect(PolygonEndInter[i], convexHull);
              var graphic = new Graphic(convexHull, new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASHDOT,
                  new Color([100, 0, 0]), 2), new Color([255, 255, 0, 0.25])
              ));
              this.map.graphics.add(graphic);
            }
            */


          }
          //var convexHullP = GeometryEngine.union(PolygonEndInter);
          /*
          for (var i = 0; i < PolygonEndInter.length; i++) {

            var convexHull = GeometryEngine.intersect(PolygonEndInter[i], convexHullP[0]);

            const popupTemplate = {
              title: "Zona de Amenaza",
              content: "Zona de Amenaza Alta"
            }
            const attributes = {
              Name: "Graphic",
              Description: "I am a polygon"
            }
            var graphic = new Graphic(convexHull, new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
              new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASHDOT,
                new Color([255, 0, 0]), 2), new Color([255, 255, 0, 0.25])
            ), attributes, popupTemplate);
            this.map.graphics.add(graphic);


          }
          */
        }



        /*
        var convexHull = GeometryEngine.intersect(PolygonEnd,newpolygon );
        debugger
        var outlineSymbol = new SimpleLineSymbol();

        outlineSymbol.setWidth(2);
        outlineSymbol.setColor(new Color([0, 115, 76, 1]));

        var markerSymbol = new SimpleMarkerSymbol();
        markerSymbol.setColor(new Color([0, 255, 197, 0.50]));
        markerSymbol.setAngle(1);
        markerSymbol.setOutline(outlineSymbol);
        markerSymbol.setSize(24);
        var graphic = new Graphic(convexHull[0], new SimpleFillSymbol());
        this.map.graphics.clear();
        this.map.graphics.add(graphic);
        */
        /*

        perturbPolyFl.applyEdits(null, [feat]).then(lang.hitch(this, function (adds, upds) {
          this.successMsg()
        })).otherwise(lang.hitch(this, function (err) {
          this.successMsg()
        }))
        */
      },

      _getIntersectZonasRiesgo: async function (polygon) {
        var query = new esri.tasks.Query();

        query.geometry = polygon
        query.where = "AMENAZA='" + this.Amenaza.value + "'";
        debugger
        let Amenzas = this.selectCapa.value
        query.outFields = ["AMENAZA"];
        query.returnGeometry = true;
        query.spatialRelationship = Query.SPATIAL_REL_INTERSECTS;
        let AmenazasLayer = new esri.layers.FeatureLayer(Amenzas);
        var resultAmenazas = await AmenazasLayer.queryFeatures(query);
        return resultAmenazas
      },

      _getIntersectCuerposAgua: async function (polygon) {
        var query = new esri.tasks.Query();

        query.geometry = polygon
        debugger
        let CueposAgua = this.selectCapa.value
        query.outFields = ["NOMBRE"];
        query.returnGeometry = true;
        query.spatialRelationship = Query.SPATIAL_REL_INTERSECTS;
        let CuerposLayer = new esri.layers.FeatureLayer(CueposAgua);
        var resultCuerpos = await CuerposLayer.queryFeatures(query);
        return resultCuerpos
      },

      successMsg: function () {
        /*
        // actualizar el estado del reporte ha asignado
        let perturbPointFl = new esri.layers.FeatureLayer(this.appConfig.perturbacionesConfig.pointLyr)
        let query = new esri.tasks.Query();
        query.where = "CODIGO_REPORTE = '" + this.featSelected.attributes.CODIGO_REPORTE + "'"
        query.outFields = ["OBJECTID", "ESTADO_REPORTE"]
        perturbPointFl.queryFeatures(query, lang.hitch(this, function (featureSet) {
          let feat = featureSet.features[0]
          feat.attributes.ESTADO_REPORTE = "ASI"
          perturbPointFl.applyEdits(null, [feat])

          let popup = new Popup({
            hasTitle: false,
            width: 340,
            maxHeight: 200,
            autoHeight: true,
            content: "Perturbación asignada exitosamente",
            buttons: [{
              label: "Ok",
              onClick: lang.hitch(this, function () {
                popup.close()
              })
            }]
          })

          this.lblPertSelected.innerHTML = "Clic en el mapa sobre la perturbación"
          this.btnAsignacion.disabled = true
        }), function (err) {
          alert('Error al consultar el reporte de la perturbación')
          console.log(err)
        })
        */
      },

      loadEjecutores: function () {

        var Buffer = [100, 250, 300, 400, 500]

        for (var i = 0; i < Buffer.length; i++) {
          let select = this.selectEjecutores
          let opt = document.createElement("option")
          opt.value = Buffer[i]
          opt.innerHTML = Buffer[i]

          select.appendChild(opt)
        }

        /*
        var selecciones = ["No", "Si"]

        for (var i = 0; i < selecciones.length; i++) {
          let select = this.selecTipo
          let opt = document.createElement("option")
          opt.value = selecciones[i]
          opt.innerHTML = selecciones[i]

          select.appendChild(opt)
        }
        */


        var dict = new Object();
        var options = [
          { label: "", value: "" },
          { label: "Zonas de amenaza Urbana", value: "https://serviciosgis.catastrobogota.gov.co/otrosservicios/rest/services/emergencias/gestionriesgos/MapServer/2" },
          { label: "Zonas de amenaza Rural", value: "https://serviciosgis.catastrobogota.gov.co/otrosservicios/rest/services/emergencias/gestionriesgos/MapServer/1" },
          { label: "Cuerpos de Agua", value: "https://serviciosgis.catastrobogota.gov.co/arcgis/rest/services/Mapa_Referencia/Mapa_Referencia/MapServer/29" }

        ]
        for (var i = 0; i < options.length; i++) {
          let select = this.selectCapa
          let opt = document.createElement("option")
          opt.value = options[i]["value"]
          opt.innerHTML = options[i]["label"]
          select.appendChild(opt)
        }

        var dict = new Object();
        var options = [
          { label: "", value: "" },
          { label: "Cantidad de niños", value: "finalamenoresshombres" },
          { label: "Cantidad de niñas", value: "finalamenoressmujeres" },
          { label: "Código del predio", value: "CODIGO_PREDIO" },
          { label: "Calificación del predio", value: "Total" },

        ]
        for (var i = 0; i < options.length; i++) {
          let select = this.Campo
          let opt = document.createElement("option")
          opt.value = options[i]["value"]
          opt.innerHTML = options[i]["label"]
          select.appendChild(opt)
        }

        var dict = new Object();
        var options = [
          { label: "", value: "" },
          { label: ">=", value: ">=" },
          { label: "<=", value: "<=" },
          { label: "=", value: "=" }

        ]
        for (var i = 0; i < options.length; i++) {
          let select = this.consulta
          let opt = document.createElement("option")
          opt.value = options[i]["value"]
          opt.innerHTML = options[i]["label"]
          select.appendChild(opt)
        }



        var TipoAmenaza = ["Amenaza Alta", "Amenaza Media", "Amenaza Baja"]
        for (var i = 0; i < TipoAmenaza.length; i++) {
          let select = this.Amenaza;
          let opt = document.createElement("option")
          opt.value = TipoAmenaza[i]
          opt.innerHTML = TipoAmenaza[i]
          select.appendChild(opt)
        }
      }
    });
    return clazz;
  });